import { Injectable } from '@angular/core';
import {Adduser} from '../model/adduser';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AddUserService {

   private url ='http://127.0.0.1:8000/api/user';

   constructor(private htpp:HttpClient) { }

   createUser(adduser:Adduser){

    return this.htpp.post(`${this.url}/user.json`,adduser)
    
  }

}
