import { Component, OnInit } from '@angular/core';
import { UserService } from '../serviceUser/user.service';
import { HttpClient } from '@angular/common/http';
import { User } from '../model/user';



@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  users: User[];
  current_user: User;

  crud_operation: { is_new: false, is_visible: false }

  url = 'http://127.0.0.1:8000/api/user';

  constructor(private userService: UserService) {
    this.users = [];

  }

  ngOnInit(): void {

    this.userService.read().subscribe( res =>{
      this.users = res.json();
      this.current_user = new User();

    });


  }


}
