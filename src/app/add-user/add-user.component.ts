import { Component, OnInit } from '@angular/core';
import { Adduser } from '../model/adduser';
import { NgForm } from '@angular/forms';
import { AddUserService } from '../serviceAddUser/add-user.service';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {

  adduser = new Adduser();

  constructor(private adduserservice: AddUserService) { }

  ngOnInit(): void {
  }

  guardar(form: NgForm) {
    if (form.invalid) {
      console.log('Formulario no validor');
      return;
    }

    this.adduserservice.createUser(this.adduser)
      .subscribe(resp => {
        console.log(resp);
      });

  }

}
