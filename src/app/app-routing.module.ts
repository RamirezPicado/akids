import { NgModule, Component } from '@angular/core';
import {Routes, RouterModule} from   '@angular/router';


import { VideoComponent } from './video/video.component';
import { PlaylistComponent } from './playlist/playlist.component';
import { UserComponent } from './user/user.component';
import { AddUserComponent} from './add-user/add-user.component';




const routes: Routes=[
    {path:'',component:UserComponent},
    {path:'user',component:UserComponent},
    {path:'add-user',component:AddUserComponent},
    {path:'video',component:VideoComponent},
    {path:'playlist',component:PlaylistComponent},
    {path:'perfil',component:PlaylistComponent},
  ];
  
@NgModule({   
        imports: [RouterModule.forRoot(routes)],  
        exports: [RouterModule]
     }) 

 export class AppRoutingModule { }