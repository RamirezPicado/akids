import { BrowserModule } from '@angular/platform-browser';
import { NgModule} from '@angular/core';
import { FormsModule} from '@angular/forms';
import {Routes, RouterModule} from   '@angular/router';
import  {AppRoutingModule} from './app-routing.module';
import {HttpModule} from '@angular/http';
import {HttpClientModule} from '@angular/common/http';
import { HttpClient } from '@angular/common/http';


import {UserService} from './serviceUser/user.service';
import {VideoService } from './serviceVideo/video.service';
import {AddUserService} from './serviceAddUser/add-user.service';

import { AppComponent } from './app.component';
import {UserComponent} from './user/user.component';
import {PlaylistComponent} from './playlist/playlist.component';
import {VideoComponent} from './video/video.component';
import { AddUserComponent } from './add-user/add-user.component';
import { PerfilesComponent } from './perfiles/perfiles.component';

@NgModule({
  declarations: [
   AppComponent,
    UserComponent,
    VideoComponent,
    PlaylistComponent,
    AddUserComponent,
    PerfilesComponent
  
    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpModule,
    RouterModule,
    HttpClientModule,
   
  
   
  ],
  providers: [UserService,VideoService,AddUserService],
  bootstrap: [AppComponent]
})
export class AppModule { }